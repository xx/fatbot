interface TgUser {
    id: number
}

interface TgChat {
    id: number
}

interface TgMessage {
    message_id: number
    from?: TgUser
    date: number
    text?: string
    chat: TgChat
}

export interface TgUpdate {
    update_id: number
    message?: TgMessage
}

export interface TgReactionType {
    type: string // always "emoji"
    emoji: string
}

export interface TgSetReaction {
    method: string // always "setMessageReaction" when sent as response to webhook request
    chat_id: number
    message_id: number
    reaction: Array<TgReactionType>
}
