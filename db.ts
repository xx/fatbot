import { Client } from "https://deno.land/x/postgres@v0.17.0/mod.ts";
import { Config } from "./config.ts";

export async function initDb(config: Config): Promise<Client> {
    const client = new Client({
        user: config.pgUser,
        database: config.pgDb,
        hostname: config.pgHostname,
        port: config.pgPort,
        password: config.pgPassword,
    });

    await client.connect();

    // await client.queryArray("CREATE TABLE IF NOT EXISTS fat_data (" +
    //         "ts TIMESTAMP NOT NULL," +
    //         "value FLOAT NOT NULL," +
    //         "user_id INT NOT NULL" +
    //     ");");

    return client;
}
