export type Config = {
    tgChatId: number   // env FAT_CHAT        - the chat ID where the bot should listen
    pgUser: string     // env FAT_PG_USER     - postgres username
    pgDb: string       // env FAT_PG_DB       - postgres database name
    pgHostname: string // env FAT_PG_HOSTNAME - postgres hostname
    pgPort: number     // env FAT_PG_PORT     - postgres port
    pgPassword: string // env FAT_PG_PASSWORD - postgres password
    isDebug: boolean   // env FAT_DEBUG       - if set, prints extra information
    // the following are set programmatically
    webhookUrl: string
    tgSecretToken: string
    tgApiUrl: string
}


function getEnv(key: string): string {
    const value = Deno.env.get(key);
    if (value === undefined) {
        console.error(`Environment variable ${key} is undefined. Please set it.`);
        Deno.exit(1);
    }
    return value;
}

function getEnvOrDefault(key: string, defaultValue: string): string {
    const value = Deno.env.get(key);
    return value === undefined ? defaultValue : value;
}

export function initConfig(): Config {
    return {
        webhookUrl: `${getEnv("FAT_HOST")}/hook/${crypto.randomUUID()}`,
        tgSecretToken: crypto.randomUUID(),
        tgApiUrl: `https://api.telegram.org/bot${getEnv("FAT_BOT_TOKEN")}`,
        tgChatId: parseInt(getEnv("FAT_CHAT")),
        pgUser: getEnv("FAT_PG_USER"),
        pgDb: getEnv("FAT_PG_DB"),
        pgHostname: getEnvOrDefault("FAT_PG_HOSTNAME", "localhost"),
        pgPort: parseInt(getEnvOrDefault("FAT_PG_PORT", "5432")),
        pgPassword: getEnv("FAT_PG_PASSWORD"),
        isDebug: Deno.env.has("FAT_DEBUG"),
    };
}
