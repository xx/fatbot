export const secretHeader = "X-Telegram-Bot-Api-Secret-Token";

export const defaultHeaders = {
    "Content-Type": "application/json; charset=utf-8"
}

export const notFound = new Response(
    JSON.stringify({message: "not found"}),
    {status: 404, headers: defaultHeaders},
);

export const ok = new Response(
    JSON.stringify({message: "ok"}),
    {headers: defaultHeaders},
);
