import { initConfig } from "./config.ts";
import { initReqHandler } from "./http.ts";
import { setupWebhook } from "./bot.ts";

if (import.meta.main) {
    const config = initConfig();
    if (config.isDebug)
        console.log({config: config});
    void setupWebhook(config);
    Deno.serve(await initReqHandler(config));
}
