import { Config } from "./config.ts";
import { defaultHeaders } from "./const.ts";
import {TgSetReaction} from "./models.ts";

export async function setupWebhook(config: Config) {
    const req = new Request(`${config.tgApiUrl}/setWebhook`, {
        method: "POST",
        headers: defaultHeaders,
        body: JSON.stringify({
            url: config.webhookUrl,
            allowed_updates: ["message"],
            secret_token: config.tgSecretToken,
        }),
    });

    const res = await fetch(req);
    if (config.isDebug)
        console.log({res: res, body: await res.json()});
}

export function reaction(ok: boolean, chat_id: number, message_id: number): string {
    const payload: TgSetReaction = {
        method: "setMessageReaction",
        chat_id: chat_id,
        message_id: message_id,
        reaction: [{ type: "emoji", emoji: ok ? "💯" : "🤯" }]
    }

    return JSON.stringify(payload)
}
